$(document).ready(function() {
	// Show/hide the flagged user list based on the filter typs
	function toggleUserList(){
		filterType = $("#edit-filter-type").val();
		if (filterType == 'everyone') {
			$("#flagged-user-list").hide();
			$("#edit-add-user-wrapper").hide();
		}
		else {
			$("#flagged-user-list").show();
			$("#edit-add-user-wrapper").show();
		}
	}
	toggleUserList();

	var originalValue = $("#edit-filter-type").val();
	var alreadyBeenChanged = false; // We only need to show the warning the first time
	$("#edit-filter-type").change(function(){
		//alert('originalValue = '+originalValue+'\n'+'alreadyBeenChanged = '+alreadyBeenChanged+'\n'+'value = '+$(this).val());
		toggleUserList();
		if($(this).val() != originalValue && alreadyBeenChanged == false){
			// Make it pink
			$("#edit-filter-type-wrapper").addClass("change-previous");
			// Add the star
			$("#edit-filter-type-wrapper label").append('<span class="warning">*</span>');	
			// Add the message
			$("#edit-update-user-details").after('<div id="filter-type-change-warning" class="warning" style="display:none;"><span class="warning">*</span>Changes will not be saved until the user details are updated.</div>');
			// Fade in the message
			$("#filter-type-change-warning").fadeIn('slow');
			// Set already been changed to true
			alreadyBeenChanged = true;
		}else if($(this).val() != originalValue && alreadyBeenChanged == true){
			// Make it pink
			$("#edit-filter-type-wrapper").addClass("change-previous");
			// Show the star
			$("#edit-filter-type-wrapper label span.warning").show();
			// Show the warning
			$("#filter-type-change-warning").fadeIn('slow');		
		}else{
			// Remove the pink
			$("#edit-filter-type-wrapper").removeClass("change-previous");
			// Hide the star
			$("#edit-filter-type-wrapper label span.warning").hide();
			// Hide the warning
			$("#filter-type-change-warning").hide();
		}
	});
	
	// "Check all" functionality	
	$(".check-all").click(function() {
		var nodeType = $(this).attr('node_type');		
		$('tr.'+nodeType+' :input').each(function(){
			this.checked = true;
		}); 
		return false;
	});
	// "Uncheck all" functionality	
	$(".check-all").click(function() {
		var nodeType = $(this).attr('node_type');		
		$('tr.'+nodeType+' :input').each(function(){
			this.checked = true;
		}); 
		return false;
	});
	$(".check-none").click(function() {
		var nodeType = $(this).attr('node_type');		
		$('tr.'+nodeType+' :input').each(function(){
			this.checked = false;
		}); 
		return false;
	});
});